#ifndef BLOCK_H
#define BLOCK_H
#include "centralDataBase.h"
#include <openssl/sha.h>

//EXERCICE 7
//Structures

typedef struct block{
  Key* author;
  CellProtected* votes;
  unsigned char* hash;
  unsigned char* previous_hash;
  int nonce;
}Block;

//Fonctions
void write_block_file (Block* block, char* file);

Block* read_block(char* filename);

char* block_to_str(Block* block);

unsigned char* hash_value(const unsigned char* s);

void compute_proof_of_work(Block* B, int d);

int verify_block(Block* B, int d);

void delete_block(Block* b);

void free_block(Block* b);

char* read_hash(char* filename);

#endif
