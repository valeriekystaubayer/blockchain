#include "centralDataBase.h"

//EXERCICE 5 - LECTURE DE STOCKAGE DES DONNEES DANS LES LISTES CHAINEES

//Q5.1
CellKey* create_cell_key(Key* key){
    CellKey* cellKey = malloc(sizeof(CellKey));
    if(cellKey == NULL){
      printf("Erreur d'allocation\n");
      return 0;
    }
    cellKey->data = key;
    cellKey->next = NULL;
    return cellKey;
}

//Q5.2
CellKey* add_cell_key(Key* key, CellKey* list){
  CellKey* cellKey = create_cell_key(key);
  cellKey->next = list;
  return cellKey;
}

//Q5.3
CellKey* read_public_keys(char* file){
  CellKey* list = NULL;
  FILE* f = fopen(file, "r");
  char line[256];
  char str[256];

  while (fgets(line, 256, f)){
    sscanf(line, "%s", str);
    Key* key = str_to_key(str);
    list = add_cell_key(key, list);
  }
  fclose(f);
  return list;
}

//Q5.4
void print_list_keys(CellKey* LCK){
  while (LCK) {
    char* key = key_to_str(LCK->data);
    printf("%s\n", key);
    LCK = LCK->next;
    free(key);
  }
}

//Q5.5
void delete_cell_key(CellKey* c){
  free(c->data);
  free(c);
}

void delete_list_key(CellKey* cellKey){
  while(cellKey){
    CellKey* next = cellKey->next;
    delete_cell_key(cellKey);
    cellKey = next;
  }
  free(cellKey);
}

//Q5.6
CellProtected* create_cell_protected(Protected* pr){
  CellProtected* cellProtected = malloc(sizeof(CellProtected));
  if(cellProtected == NULL){
    printf("Erreur d'allocation\n");
    return 0;
  }
  cellProtected->data = pr;
  cellProtected->next = NULL;
  return cellProtected;
}

//Q5.7
CellProtected* add_cell_protected(Protected* pr, CellProtected* list){
  CellProtected* cellProtected = create_cell_protected(pr);
  cellProtected->next = list;
  return cellProtected;
}

//Q5.8
CellProtected* read_protected(char* file){
  CellProtected* list = NULL;
  FILE* f = fopen(file, "r");
  char line[256];

  while (fgets(line, 256, f)){
    Protected* pr = str_to_protected(line);
    list = add_cell_protected(pr, list);
  }
  fclose(f);
  return list;
}

//Q5.9
void print_list_protected(CellProtected* LCP){
  while (LCP){
    char* str = protected_to_str(LCP->data);
    printf("%s \n", str);
    LCP = LCP->next;
    free(str);
  }
}

//Q5.10
void delete_cell_protected(CellProtected* c){
    free_protected(c->data);
    free(c);
}

void delete_list_protected(CellProtected* cellProtected){
    while(cellProtected){
        CellProtected* next = cellProtected->next;
        delete_cell_protected(cellProtected);
        cellProtected = next;
  }
  free(cellProtected);
}

//EXERCICE 6 - DETERMINATION DU GAGNANT DE L'ELECTION

//Q6.1
void verify_list_cell(CellProtected** cellProtected){
  CellProtected* first = *cellProtected;
  if((*cellProtected != NULL) && (!verify((*cellProtected)->data))){
    first = (*cellProtected)->next;
    delete_cell_protected(*cellProtected);
    *cellProtected = first;
  }
  while(*cellProtected){
    CellProtected* next = (*cellProtected)->next;
    if((next != NULL) && (!verify(next->data))){
      (*cellProtected)->next = next->next;
      delete_cell_protected(next);
    }
    *cellProtected = (*cellProtected)->next;
  }
  *cellProtected = first;
}

//Q6.2
HashCell* create_hashcell(Key* key){
  HashCell* hc = malloc(sizeof(HashCell));
  if(hc == NULL){
    printf("Erreur d'allocation\n");
    return 0;
  }
  hc->key = copy_key(key);
  hc->val = 0;
  return hc;
}

//Q6.3
int hash_function(Key* key, int size){
  return abs(size*((key->val*(sqrt(5)-1)/2)-(abs(key->val*(sqrt(5)-1)/2))));
}

//Q6.4
int find_position(HashTable* t, Key* key){
  int i = hash_function(key, t->size);
  while(t->tab[i] != NULL && !equal_key(key, t->tab[i]->key)){
    i = (i + 1) % t->size;
  }
  return i;
}

//Q6.5
HashTable* create_hashtable(CellKey* keys, int size){
  HashTable* ht = malloc(sizeof(HashTable));
  if(ht == NULL){
    printf("Erreur d'allocation\n");
    return 0;
  }
  ht->size = size;
  ht->tab = malloc(size*sizeof(HashCell*));
  for(int i = 0; i < size; i++){
    ht->tab[i] = NULL;
  }
  if(ht->tab == NULL){
    printf("Erreur d'allocation\n");
    return 0;
  }
  while(keys){
    int i = find_position(ht, keys->data);
    ht->tab[i] = create_hashcell(keys->data);
    keys = keys->next;
  }
  return ht;
}

//Q6.6
void delete_hashtable(HashTable* t){
  for(int i = 0; i < t->size; i++){
    free(t->tab[i]->key);
    free(t->tab[i]);
  }
  free(t->tab);
  free(t);
}

//Q6.7
Key* compute_winner(CellProtected* decl, CellKey* candidates, CellKey* voters, int sizeC, int sizeV){
  verify_list_cell(&decl);
  HashTable* Hc = create_hashtable(candidates, sizeC);
  HashTable* Hv = create_hashtable(voters, sizeV);
  while(decl){
    int i = find_position(Hv, decl->data->pKey);
    if(Hv->tab[i] != NULL && equal_key(decl->data->pKey, Hv->tab[i]->key) && Hv->tab[i]->val == 0){
      Key* vKey = str_to_key(decl->data->mess);
      int j = find_position(Hc, vKey);
      if(Hc->tab[j] != NULL && equal_key(vKey, Hc->tab[j]->key)){
        Hc->tab[j]->val += 1;
        Hv->tab[i]->val = 1;
      }
      free(vKey);
    }
    decl = decl->next;
  }
  HashCell* wHC = create_hashcell(Hc->tab[0]->key);
  wHC->val = Hc->tab[0]->val;
  for(int i = 1; i < sizeC; i++){
    if(Hc->tab[i]->val > wHC->val){
      free(wHC->key);
      wHC->key = copy_key(Hc->tab[i]->key);
      wHC->val = Hc->tab[i]->val;
    }
  }
  Key* wKey = copy_key(wHC->key);
  free(wHC->key);
  free(wHC);
  delete_hashtable(Hc);
  delete_hashtable(Hv);
  return wKey;
}
