#include "declarations.h"

int main(){
	srand(time(NULL));

	//Test Declaration Securisees
	//Testing Init Keys
	printf("\nTesting Init Keys\n");
	Key *pKey = malloc(sizeof(Key));
	Key *sKey = malloc(sizeof(Key));
	if( pKey == NULL || sKey == NULL){
		printf("Erreur d'allocation\n");
	}

	init_pair_keys(pKey, sKey, 3, 7);
	printf("cle publique = (%ld, %ld) \n", pKey->val, pKey->n );
	printf("cle privee = (%ld, %ld) \n", sKey->val, sKey->n);

	//Testing Key Serialisation
	printf("\nTesting Key Serialization\n");
	char* chaine = key_to_str(pKey);
	printf("clé publique en hexadecimal: %s\n", chaine);
	Key* key = str_to_key(chaine);
	printf("clé publique: (%ld, %ld) \n", key->val, key->n);

	free(chaine);
	free(key);

	//Testing signature
	//Candidate keys:
	printf("\nTesting signature\n");
	Key* pKeyC = malloc (sizeof(Key));
	Key* sKeyC = malloc (sizeof( Key ));
	if(pKeyC == NULL || sKeyC == NULL){
		printf("Erreur d'allocation\n");
	}
	init_pair_keys ( pKeyC , sKeyC ,3 ,7);

	free(sKeyC);

	//Declaration:
	char* mess = key_to_str(pKeyC) ;
	char* m = key_to_str(pKey);
	printf ("%s vote pour %s \n", m, mess);
	Signature * sgn = sign(mess, sKey);
	printf ("signature: ") ;
	print_long_vector (sgn->content, sgn->size);

	free(m);

	chaine = signature_to_str(sgn);
	printf ("signature to str: %s \n", chaine);
	free_signature(sgn);
	sgn = str_to_signature(chaine);
	printf ("str to signature: ");
	print_long_vector (sgn->content, sgn->size);
	free(chaine);

	//Testing protected:
	printf("\nTesting protected\n");
	Protected* pr = init_protected(pKeyC, mess, sgn);

	//Verification:
	if (verify(pr)){
		printf ("Signature valide \n") ;
	}
	else {
		printf ("Signature non valide \n") ;
	}

	chaine = protected_to_str(pr);
	printf ("protected to str: %s\n", chaine);
	free_protected(pr);
	pr = str_to_protected(chaine);
	char* strkey = key_to_str(pr->pKey);
	char* strsgn = signature_to_str(pr->sgn);
	printf ("str to protected: %s %s %s\n", strkey, pr->mess, strsgn);

	free(pKey);
	free(sKey);
	free(chaine);
	free(strkey);
	free(strsgn);
	free_protected(pr);

	generate_random_data(10,10);

	return 0;
}
