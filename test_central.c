#include "centralDataBase.h"

int main(){
  srand(time(NULL));

  generate_random_data(10,10);

  //Testing CellKeys
  Key *key1 = malloc(sizeof(Key));
  Key *key2 = malloc(sizeof(Key));
  if(key1 == NULL || key2 == NULL){
    printf("Erreur d'allocation\n");
  }
  init_pair_keys(key1, key2, 3, 7);
  char* strkey1 = key_to_str(key1);
  char* strkey2 = key_to_str(key2);
  printf("cle 1 = %s\n", strkey1);
  printf("cle 2 = %s\n", strkey2);
  printf("\nTesting Cellkey\n");
  CellKey* list = create_cell_key(key1);
  printf("\nAjout d'une clé dans la liste et son affichage:\n");
  list = add_cell_key(key2, list);
  print_list_keys(list);

  delete_list_key(list);
  free(strkey1);
  free(strkey2);


  //Testing read public keys
  printf("\nLecture des fichiers et affichage de la liste 1\n");
  CellKey* l1 = read_public_keys("Fichiers/keys.txt");
  print_list_keys(l1);

  //Testing cell deletion
  printf("\nTesting cell deletion\n");
  printf("\nAvant la suppresion:\n");
  printf("Liste 1:\n");
  print_list_keys(l1);
  printf("\nSuppression de la liste...\n");
  delete_list_key(l1);


  //Testing read protected
  printf("\nLecture des fichiers et affichage de la liste:\n");
  CellProtected* l2 = read_protected("Fichiers/declarations.txt");
  print_list_protected(l2);

  //Testing verify list cell
  printf("\nTesting verify list cell\n");
  Key *key = malloc(sizeof(Key));
  init_key(key, 3, 7);
  free(l2->data->pKey);
  l2->data->pKey = key;
  printf("liste fausse avant verify:\n");
  print_list_protected(l2);
  verify_list_cell(&l2);
  printf("liste apres verify:\n");
  print_list_protected(l2);
  delete_list_protected(l2);

  CellKey* lk = read_public_keys("Fichiers/keys.txt");
  CellKey* lc = read_public_keys("Fichiers/candidates.txt");
  CellProtected* ld = read_protected("Fichiers/declarations.txt");

  //Testing HashCell
  printf("\nTesting HashCell avec val à 0:\n");
  HashCell* hc = create_hashcell(lk->data);
  char* strkey = key_to_str(hc->key);
  printf("key : %s | val : %d\n", strkey, hc->val);
  free(strkey);
  free(hc->key);
  free(hc);

  //Testing HashTable
  printf("\nTesting HashTable :\n");
  HashTable* ht = create_hashtable(lk, 10);
  for(int i = 0; i < 10; i++){
    char* strkey = key_to_str(ht->tab[i]->key);
    printf("key : %s | val : %d\n", strkey, ht->tab[i]->val);
    free(strkey);
  }
  
  //Testing free hashtable  
  delete_hashtable(ht);

  printf("\nTesting compute_winner:\n");
  Key* k = compute_winner(ld, lc, lk, 10, 10);
  strkey = key_to_str(k);
  printf("Gagnant : %s\n", strkey);
  free(strkey);
  free(k);

  delete_list_key(lk);
  delete_list_key(lc);
  delete_list_protected(ld);

  return 0;

}
