#include "blockchain.h"

//EXERCICE 9

//Q9.1
void submit_vote(Protected* p){
  FILE* f = fopen("Fichiers/Pending_votes.txt", "a");
  if (f == NULL){
    printf("Erreur d'ouverture d'un fichier dans submit_vote\n");
    exit(0);
  }

  char* strp = protected_to_str(p);
  fprintf(f, "%s", strp);

  free(strp);
  fclose(f);
}

//Q9.2
void create_block(char* previous_hash, Key* author, int d){
  Block* b = malloc(sizeof(Block));
  if (b == NULL){
    printf("Erreur d'allocation dans create_block");
    exit(0);
  }

  CellProtected* lp = read_protected("Fichiers/Pending_votes.txt");

  b->author = author;
  b->votes = lp;
  b->hash = (unsigned char*)"";
  b->previous_hash = (unsigned char*)strdup(previous_hash);
  b->nonce = 0;
  compute_proof_of_work(b, d);
  remove("./Fichiers/Pending_votes.txt");
  write_block_file(b, "./Fichiers/Pending_block");
  delete_block(b);
  delete_list_protected(lp);
}

//Q9.3
void add_block(int d, char* name){
  Block* b = read_block("./Fichiers/Pending_block");
  if(verify_block(b, d)){
    char rename[256];
    sprintf(rename, "./Fichiers/Blockchain/%s", name);
    write_block_file(b, rename);
  }
  remove("./Fichiers/Pending_block");
  Key* k = b->author;
  CellProtected* lp = b->votes;
  free(k);
  delete_list_protected(lp);
  delete_block(b);
}

//Q9.4
CellTree* read_tree(int sizeT){
  CellTree** T = malloc(sizeT*sizeof(CellTree*));
  int i = 0;

  DIR *rep = opendir("./Fichiers/Blockchain/");
  if (rep != NULL){
    struct dirent* dir;
    while ((dir = readdir(rep))) {
      if (strcmp(dir->d_name, ".") != 0 && strcmp(dir->d_name, "..") != 0){
        char name[278];
        sprintf(name, "./Fichiers/Blockchain/%s", dir->d_name);
        Block* b = read_block(name);
        T[i] = create_node(b);
        i++;
      }
    }
    closedir(rep);
  }
  int size = i;

  for(i = 0; i < size; i++){
    for(int j = 0; j < size; j++){
      char hash[256];
      char previous_hash[256];
      sprintf(hash, "%s", (char*)T[i]->block->hash);
      sprintf(previous_hash, "%s", (char*)T[j]->block->previous_hash);
      if(strcmp(hash, previous_hash) == 0){
        add_child(T[i], T[j]);
      }
    }
  }

  for (i = 0; i < size; i++) {
    if(T[i]->father == NULL){
      break;
    }
  }
  CellTree* tree = T[i];
  free(T);
  return tree;
}

//Q9.5
Key* compute_winner_BT(CellTree* tree, CellKey* candidates, CellKey* voters, int sizeC, int sizeV){
  CellProtected* decl = liste_votes(tree);
  verify_list_cell(&decl);
  Key* wKey = compute_winner(decl, candidates, voters, sizeC, sizeV);
  delete_list_protected(decl);
  return wKey;
}
