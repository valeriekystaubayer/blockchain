#include "encrypt.h"

//EXERCICE 1 - PRIMALITE

//Q1.1 - is prime?
int is_prime_naive(long p){
  if (p == 0 || p == 1){
    return 0;
  }
  for(int i = 2; i < p; i ++){
    if (p % i == 0){
      return 0;
    }
  }
  return 1;
}

//EXPONENTION MODULAIRE RAPIDE

//Q1.3 - retourne la valeur a^b % n
int modpow_naive(long a, long m, long n){
  long res = a;
  for(long b = 1; b <= m; b++){
    res = res * a;
  }
  return res % n;
}


//Q1.4 - modpow recursif
int modpow(long a,long m,long n){
    if (m == 0){
        return 1;
    }
    if (m == 1){
    return a % n;
    }
    long b = modpow(a, (long)m/2, n);
    if (m%2 == 0){
    return (b * b) % n;
    }
    return (a * b * b) % n;
    }

//TEST DE MILLER-RABIN

int witness(long a,long b,long d,long p){
  long x = modpow(a,d,p);
  if(x == 1){
    return 0;
  }
  for(long i = 0; i < b; i++){
    if(x == p-1){
      return 0;
    }
    x = modpow(x,2,p);
  }
  return 1;
}


long rand_long(long low,long up){ //un entier long genere aleatoirement entre low et up inclus.
  return rand() % (up - low + 1) + low;
}

//Realisation du test
int is_prime_miller(long p,int k){
  if(p == 2){
    return 1;
  }
  if(!(p & 1) || p <= 1){ //on verifie que p est impair et different de 1
    return 0;
  }
  long b = 0;
  long d = p - 1;
  while(!(d & 1)){ //tant que d n’est pas impair
    d = d / 2;
    b = b + 1;
  }
  // On genere k valeurs pour a, et on teste si c’est un temoin:
  long a;
  int i;
  for(i = 0; i < k; i++){
    a = rand_long(2, p - 1);
    if(witness(a,b,d,p)){
      return 0;
    }
  }
  return 1;
}

//Q1.8 - generation d'un entier premier selon le test de primalite de Miller-Rabin

long random_prime_number(int low_size, int up_size, int k){
    int x = 0, a = 2, b = 2;
    long r;
    for(int i = 1; i <= (low_size - 1); i++){
    a = a * 2;
    }
    for(int j = 1; j <= up_size; j++){
    b = b * 2;
    }
    while(x == 0){
    r = rand_long(a, b - 1);
    x = is_prime_miller(r, k);
    }
    return r;
}

//EXERCICE 2 - IMPLEMENTATION DU PROTOCOLE RSA

//L'alorithme d'Euclide pour determiner la valeur PGCD(s,t) = s * u + t * v
long extended_gcd(long s, long t, long *u, long *v){
  if (s == 0){
    *u = 0;
    *v = 1;
    return t;
  }
  long uPrim, vPrim;
  long gcd = extended_gcd(t % s, s, &uPrim, &vPrim);
  *u = vPrim - (t / s) * uPrim;
  *v = uPrim;
  return gcd;
}

//Q2.1 - Generation de la cle plublique pkey et la cle secrete skey
void generate_key_values(long p, long q, long *n, long *s, long *u){
  *n = p * q;
  long t = (p - 1) * (q - 1);
  *s = rand_long(0, t - 1);
  long v;
  while(extended_gcd(*s, t, u, &v) != 1){
    *s = rand_long(0, t - 1);
  }
}
//CHIFFREMENT ET DECHIFFREMENT DE MESSAGES
long* encrypt(char* chaine, long s, long n){ //chiffre la chaine de caracteres chaine avec la cle publique pKey = (s, n)
  long* tab = malloc(strlen(chaine) * sizeof(long));
  if(tab == NULL){
    printf("Erreur d'allocation\n");
    return 0;
  }
  for (int i = 0; *chaine != '\0'; i++){
    tab[i] = modpow((int) * chaine, s, n);
    chaine += 1;
  }
  return tab;
}

char* decrypt(long* crypted, int size, long u, long n){ //dechiffre un message a l’aide de la cle secrete skey = (u, n)
  char *chaine = malloc((size + 1) * sizeof(char));
  if(chaine == NULL){
    printf("Erreur d'allocation\n");
    return 0;
  }
  for(int i = 0; i < size; i++){
    chaine[i] = modpow(crypted[i], u, n);
  }
  chaine[size] = '\0';
  return chaine;
}

//Fonction d'affichage

void print_long_vector(long *result, int size){
  printf("Vector: [ ");
  for (int i = 0; i < size; i++){
    printf("%ld \t", result[i]);
  }
  printf("]\n");
}
