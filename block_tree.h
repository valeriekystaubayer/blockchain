#ifndef BLOCK_TREE_H
#define BLOCK_TREE_H
#include "block.h"

//fonction renvoie la valeur maximale entre les deux arguments passé en param
#define MAX(X, Y) (((X) > (Y)) ? (X) : (Y)) 

//EXERCICE 8
//Structures

typedef struct block_tree_cell{
  Block* block ;
  struct block_tree_cell* father;
  struct block_tree_cell* firstChild;
  struct block_tree_cell* nextBro;
  int height;
}CellTree;

//Fonctions
CellTree* create_node(Block* b);

int update_height(CellTree* father, CellTree* child);

void add_child(CellTree* father, CellTree* child);

void print_tree(CellTree* t);

void delete_node(CellTree* node);

void delete_tree(CellTree* t);

CellTree* highest_child(CellTree* cell);

CellTree* last_node(CellTree* tree);

CellProtected* fus_liste_protected(CellProtected* liste1, CellProtected* liste2);

CellProtected* liste_votes(CellTree* tree);

#endif
