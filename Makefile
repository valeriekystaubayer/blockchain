all: main

main: main.c encrypt.o declarations.o centralDataBase.o block.o block_tree.o blockchain.o
	gcc -o main main.c encrypt.o declarations.o centralDataBase.o block.o block_tree.o blockchain.o -lssl -lcrypto -g

encrypt.o: encrypt.c encrypt.h
	gcc -Wall -c encrypt.c -g

declarations.o: declarations.c declarations.h
	gcc -Wall -c declarations.c -g

centralDataBase.o: centralDataBase.c centralDataBase.h
	gcc -Wall -c centralDataBase.c -g

block.o: block.c block.h
	gcc -Wall -c block.c -g

block_tree.o: block_tree.c block_tree.h
	gcc -Wall -c block_tree.c -g

blockchain.o: blockchain.c blockchain.h
	gcc -Wall -c blockchain.c -g

SHA256: SHA256_test.c
	gcc -Wall -o SHA_test SHA256_test.c -lssl -lcrypto -g

test_encrypt: test_encrypt.c encrypt.o
	gcc -Wall -o test_encrypt test_encrypt.c encrypt.o -g

test_declarations: test_declarations.c encrypt.o declarations.o
	gcc -Wall -o test_declarations test_declarations.c encrypt.o declarations.o -g

test_central: test_central.c encrypt.o declarations.o centralDataBase.o
	gcc -Wall -o test_central test_central.c encrypt.o declarations.o centralDataBase.o -g

test_block: test_block.c encrypt.o declarations.o centralDataBase.o block.o
	gcc -Wall -o test_block test_block.c encrypt.o declarations.o centralDataBase.o block.o -lssl -lcrypto -g

clean:
	rm -f main *.o
	find . -type f -not -iname "*.*" -and -not -iname "Makefile" -exec rm '{}' \;
