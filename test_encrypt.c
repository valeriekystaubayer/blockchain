#include "encrypt.h"

int main(){
  srand(time(NULL));

  //Comparison de temps moeyn de modpow et modpow_naive
  clock_t start, end;
  FILE* f = fopen("Fichiers/modpow.txt", "w");
  for(int i = 0; i < 1000000; i += 10000){
    //modpow_naive
    start = clock();
    modpow_naive(2, i, 8);
    end = clock();
    double cpu1 = ((double)(end - start)) / CLOCKS_PER_SEC;
    //modpow
    start = clock();
    modpow(2, i, 8);
    end = clock();
    double cpu2 = ((double)(end - start)) / CLOCKS_PER_SEC;

    fprintf(f, "%d %f %f\n", i, cpu1, cpu2);
  }

  fclose(f);

  printf("Un nombre premier généré aléatoirement: %ld", random_prime_number(3, 7, 5000));

  start = clock();
  is_prime_naive(8539);
  end = clock();
  double cpu = ((double)(end - start)) / CLOCKS_PER_SEC;

  printf("\nTemps d'execution de is_prime_naive: %f\n", cpu);

  //Generation de cle :
  long p = random_prime_number(3, 7, 5000);
  long q = random_prime_number(3, 7, 5000);
  while(p == q){
    q = random_prime_number(3, 7, 5000);
  }

  long n, s, u;
  generate_key_values(p, q, &n, &s, &u);
  //Pour avoir des cles positives :
  if (u < 0){
    long t = (p - 1) * (q - 1);
    u += t; //on aura toujours s*u mod t = 1
  }

  //Affichage des cles en hexadecimal :
  printf("cle publique = (%lx, %lx) \n", s, n);
  printf("cle privee = (%lx, %lx) \n" , u, n);

  //Chiffrement :
  char mess1[10] = "Hello";
  int len = strlen (mess1);
  long* crypted = encrypt(mess1, s, n);

  printf("Initial message: %s", mess1);

  printf("\nEncoded representation \n");
  print_long_vector(crypted, len);

  //Dechiffrement :
  char* decoded = decrypt(crypted, len, u, n);
  printf("Decoded: %s\n", decoded);

  free(crypted);
  free(decoded);

  return 0;
}
