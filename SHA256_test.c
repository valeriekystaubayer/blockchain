#include <stdlib.h>
#include <stdio.h>
#include <openssl/sha.h>
#include <string.h>

int main(){
  const unsigned char* s = "Rosetta code";
  unsigned char *d = SHA256(s, strlen((char*)s), 0);
  for (int i = 0; i < SHA256_DIGEST_LENGTH; i++){
    printf("%02x", d[i]);
  }
  putchar( '\n');

  return 0;
}
