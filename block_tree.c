#include "block_tree.h"

//EXERCICE 8
//Q8.1
CellTree* create_node(Block* b){
  CellTree* t = (CellTree*)malloc(sizeof(CellTree));
  t->block = b;
  t->father = NULL;
  t->firstChild = NULL;
  t->nextBro = NULL;
  t->height = 0;
  return t;
}

//8.2
int update_height(CellTree* father, CellTree* child){
  int t = child->height + 1;
  if (MAX(t, father->height) == father->height){ //fonction MAX definie dans .h
    return 0;
  }
  father->height = t;
  return 1;
}

//8.3
void add_child(CellTree* father, CellTree* child){
  child->father = father;
  if (father->firstChild){
    CellTree* bro = father->firstChild;
    while(bro->nextBro){
      bro = bro->nextBro;
    }
    bro->nextBro = child;
  }
  else{
    father->firstChild = child;
  }
  while(father && update_height(father, child) == 1){
    child = father;
    father = father->father;
  }
}

//8.4
void print_tree(CellTree* t){
  while(t){
    printf("Height: %02d | %s\n", t->height, t->block->hash);
    CellTree* bro = t->nextBro;
    print_tree(bro);
    t = t->firstChild;
  }
}

//8.5
void delete_node(CellTree* node){
  free(node->block->author);
  delete_block(node->block);
  free(node);
}

void delete_tree(CellTree* t){
  while (t){
    CellTree* bro = t->nextBro;
    CellTree* child = t->firstChild;
    delete_node(t);
    t = child;
    delete_tree(bro);
  }
  free(t);
}

//8.6
CellTree* highest_child(CellTree* cell){
  if(cell->firstChild == NULL){
    return NULL;
  }
  CellTree* hchild = cell->firstChild;
  CellTree* bro = hchild->nextBro;
  while(bro){
    if(hchild->height < bro->height){
      hchild = bro;
    }
    bro = bro->nextBro;
  }
  return hchild;
}

//Q8.7
CellTree* last_node(CellTree* tree){
  CellTree* ln = tree;
  while (tree) {
    ln = tree;
    tree = highest_child(tree);
  }
  return ln;
}

//Q8.8
CellProtected* fus_liste_protected(CellProtected* liste1, CellProtected* liste2){
  CellProtected* l = liste1;
  CellProtected* next = liste1->next;
  while(next){
    liste1 = next;
    next = liste1->next;
  }
  liste1->next = liste2;
  return l;
}

//Q8.9
CellProtected* liste_votes(CellTree* tree){
  CellTree* node = last_node(tree);
  CellProtected* l = NULL;
  while(node){
    l = fus_liste_protected(node->block->votes, l);
    node = node->father;
  }
  return l;
}
